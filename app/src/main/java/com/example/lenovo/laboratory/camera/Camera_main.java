package com.example.lenovo.laboratory.camera;

import android.app.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lenovo.laboratory.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by arimahardika on 21/03/2018.
 */

public class Camera_main extends Activity {

    static final int REQUEST_CODE = 1;
    ImageView imageView;
    Button button;
    String currentPhotoPath;
    String TAG = "CAMERA_MAIN";
    Uri photoURI;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_main);
        imageView = findViewById(R.id.imageView_testCamera);
        button = findViewById(R.id.btn_takepicture);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

    }

    //region Take Image
    private void dispatchTakePictureIntent() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider", photoFile);
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePicture, REQUEST_CODE);
            }
        }
    }
    //endregion

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmapLoaded = BitmapFactory.decodeFile(currentPhotoPath);
                    imageView.setImageBitmap(bitmapLoaded);
                    Toast.makeText(this, "LOAD IMAGE SUCCESS", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("PHOTOPATH", "LOAD IMAGE FAILED - " + e.toString());
                    Toast.makeText(this, "LOAD IMAGE FAILED", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        //Create Image file name here
        String timeStamp = new SimpleDateFormat("ddmmyyyy", Locale.getDefault()).format(new Date());
        String imageName = "tanggap_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Log.d(TAG, "Storage Dir : " + storageDir);
        File imageCreated = File.createTempFile(imageName, ".jpg", storageDir);
        currentPhotoPath = imageCreated.getAbsolutePath();
        Log.d(TAG, "Photo Path : " + currentPhotoPath);

        return imageCreated;
    }

}