package com.example.lenovo.laboratory.pasar;

/**
 * Created by arimahardika on 22/02/2018.
 */

public class Model_Pasar {
    private String nama, jenis, satuan, harga_kemarin;

    public Model_Pasar(String nama, String jenis, String satuan, String harga_kemarin) {
        this.nama = nama;
        this.jenis = jenis;
        this.satuan = satuan;
        this.harga_kemarin = harga_kemarin;
    }

    public String getNama() {
        return nama;
    }

    public String getJenis() {
        return jenis;
    }

    public String getSatuan() {
        return satuan;
    }

    public String getHarga_kemarin() {
        return harga_kemarin;
    }
}
