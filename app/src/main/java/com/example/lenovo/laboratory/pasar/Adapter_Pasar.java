package com.example.lenovo.laboratory.pasar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.laboratory.R;

import java.util.List;

/**
 * Created by arimahardika on 22/02/2018.
 */

public class Adapter_Pasar extends RecyclerView.Adapter<Adapter_Pasar.PasarViewHolder> {

    private static int currentPosition = 0;
    private List<Model_Pasar> listPasar;
    //dont delete, used in Main_Pasar.java
    private Context context;

    public Adapter_Pasar(List<Model_Pasar> listPasar, Context context) {
        this.listPasar = listPasar;
        this.context = context;
    }

    @Override
    public PasarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pasar_list,
                parent, false);
        return new PasarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PasarViewHolder holder, int position) {
        Model_Pasar modelPasar = listPasar.get(position);

        CharSequence getNama = modelPasar.getNama() + " " + modelPasar.getJenis();
        CharSequence getSatuan = modelPasar.getSatuan();
        CharSequence getHargaKemarin = modelPasar.getHarga_kemarin();

        holder.txt_nama.setText(getNama);
        holder.txt_satuan.setText(getSatuan);
        holder.txt_harga_kemarin.setText(getHargaKemarin);

    }

    @Override
    public int getItemCount() {
        return listPasar.size();
    }

    class PasarViewHolder extends RecyclerView.ViewHolder {
        TextView txt_nama, txt_satuan, txt_harga_kemarin;

        LinearLayout linearLayout;

        PasarViewHolder(View itemView) {
            super(itemView);

            txt_nama = itemView.findViewById(R.id.tv_pasar_nama);
            txt_satuan = itemView.findViewById(R.id.tv_pasar_satuan);
            txt_harga_kemarin = itemView.findViewById(R.id.tv_pasar_harga);

            linearLayout = itemView.findViewById(R.id.ll_pasar_linearLayout);
        }
    }
}
