package com.example.lenovo.laboratory.viewPager;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import com.example.lenovo.laboratory.R;

/**
 * Created by arimahardika on 22/02/2018.
 */

public class MainViewPager extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_main);

        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new AdapterCustomPager(this));
    }
}
