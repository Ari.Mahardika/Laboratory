package com.example.lenovo.laboratory.customListView;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.laboratory.R;

/**
 * Created by lenovo on 2/13/2018.
 */

public class CustomListViewAdapter extends BaseAdapter {

    private final Activity context;
    private final String[] title;
    private final String[] desc;
    private final int[] icon;

    public CustomListViewAdapter(Activity context, String[] title, String[] desc, int[] icon) {
        this.context = context;
        this.title = title;
        this.desc = desc;
        this.icon = icon;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return title.length;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.customlistview_customrow, null);
        }

        TextView tv_title = rowView.findViewById(R.id.textView_title);
        TextView tv_desc = rowView.findViewById(R.id.textView_desc);
        ImageView iv_icon = rowView.findViewById(R.id.imageView_icon);

        tv_title.setText(title[position]);
        tv_desc.setText(desc[position]);
        iv_icon.setImageResource(icon[position]);

        return rowView;
    }


//    public CustomListViewAdapter(Activity context, String[] title, String[] desc, int[] icon){
//        super(context, R.layout.customlistview_customrow, title);
//
//        this.context = context;
//        this.title = title;
//        this.desc = desc;
//        this.icon = icon;
//    }
//
//    @Override
//    public View getView(int position,  View convertView, ViewGroup parent) {
//        LayoutInflater inflater = context.getLayoutInflater();
//        View rowView = inflater.inflate(R.layout.customlistview_customrow, null, true);
//
//        TextView tv_title = rowView.findViewById(R.id.textView_title);
//        TextView tv_desc = rowView.findViewById(R.id.textView_desc);
//        ImageView iv_icon = rowView.findViewById(R.id.imageView_icon);
//
//        tv_title.setText(title[position]);
//        tv_desc.setText(desc[position]);
//        iv_icon.setImageResource(icon[position]);
//
//        return rowView;
//    }
}
